package ru.edu;

import java.util.concurrent.atomic.AtomicInteger;

public class MatrixService {

    /**
     * Iterates over each column and calculates sum of elements
     */
    private static class ColumnSummator implements Runnable {

        private int fromColumn;
        private int toColumn;
        private int[][] matrix;
        private int resultId;
        private int[] result;
        private AtomicInteger syncObject;

        /**
         * Constructor
         *
         * @param fromColumn - column index start with
         * @param toColumn   - to column index. You should process columns strong before column with index toColumn
         * @param matrix     - matrix
         * @param resultId   - position of result in result array
         * @param result     - result array
         * @param syncObject - object for synchronization between threads
         */
        public ColumnSummator(int fromColumn, int toColumn, int[][] matrix, int resultId, int[] result, AtomicInteger syncObject) {
            this.fromColumn = fromColumn;
            this.toColumn = toColumn;
            this.matrix = matrix;
            this.resultId = resultId;
            this.result = result;
            this.syncObject = syncObject;
        }

        @Override
        public void run() {

            System.out.println("Tread threadId=" + Thread.currentThread().getId() + " started");

            for (int i = fromColumn; i < toColumn; i++) {

                for (int j = 0; j < matrix.length; j++) {

                    result[resultId] += matrix[j][i];

                }
            }
            synchronized (syncObject) {
                syncObject.decrementAndGet();
                if(syncObject.get() == 0) {
                    syncObject.notify();
                }
            }
            System.out.println("Tread threadId=" + Thread.currentThread().getId() + " finished");
        }
    }

    /**
     * Get sum of matrix elements. You should parallel work between several threads
     *
     * @param matrix   - matrix
     * @param nthreads - threads count. It is guarantee that number of matrix column is greater than nthreads.
     * @return sum of matrix elements
     */
    public int sum(int[][] matrix, int nthreads){

        int[] result = new int[nthreads];
        AtomicInteger syncObject = new AtomicInteger(nthreads);

        int fromColumn;
        int toColumn;
        int resultId = 0;

        int value = matrix[0].length / nthreads;

        for (int k = 0; k < nthreads; k++) {

            fromColumn = value  * k;
            toColumn = fromColumn + value;
            if(k == nthreads - 1){
                toColumn = fromColumn + matrix[0].length % nthreads +value;
            }

            ColumnSummator task = new ColumnSummator(fromColumn, toColumn, matrix, resultId, result, syncObject);
            Thread thread = new Thread(task);
            thread.start();
            resultId++;

        }

    synchronized (syncObject) {
        try {
            if(syncObject.get() > 0) {
            syncObject.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

        int sum = 0;

        for (int threadResult : result) {
            System.out.println(threadResult);
            sum += threadResult;
        }

        return sum;
    }
}
