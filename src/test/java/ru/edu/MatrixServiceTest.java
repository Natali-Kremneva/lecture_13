package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixServiceTest {

    @Test
    public void sumTest() throws InterruptedException {
        MatrixService matrixService = new MatrixService();
        int[][] matrix = new int[][]{
                {1, 1},
                {2, 2},
                {3, 3},
                {4, 4},
                {10, 10}
        };

        int sum = matrixService.sum(matrix, 1);
        System.out.println("sum = " + sum);
        Assert.assertEquals(40, sum);

        int sum4 = matrixService.sum(matrix, 2);
        System.out.println("sum4 = " + sum4);
        Assert.assertEquals(40, sum4);

        int[][] matrix1 = new int[][]{
                {1, -1, 1},
                {2, -2, 2},
                {-3, 3, 3},
                {-4, 4, 4},
                {-10, 11, 5}
        };
        int sum1 = matrixService.sum(matrix1, 2);
        System.out.println("sum1 = " + sum1);
        Assert.assertEquals(16, sum1);

        int sum5 = matrixService.sum(matrix1, 3);
        System.out.println("sum5 = " + sum5);
        Assert.assertEquals(16, sum5);

        int[][] matrix2 = new int[][]{
                {1, 1, 1},
                {2, 2, 2},
                {3, 3, 3},
                {4, 4, 4},
                {10, 10, 10}
        };
        int sum2 = matrixService.sum(matrix2, 3);
        System.out.println("sum2 = " + sum2);
        Assert.assertEquals(60, sum2);
    }
}